From 869720f1706f5a2988a47c083fa5faf0ae91882c Mon Sep 17 00:00:00 2001
From: Kugi Eusebio <kugi_eusebio@protonmail.com>
Date: Mon, 12 Aug 2024 05:22:03 +0000
Subject: [PATCH] Adds a rotate button when orientation is locked

---
 qml/OrientedShell.qml | 168 ++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 168 insertions(+)

--- a/qml/OrientedShell.qml
+++ b/qml/OrientedShell.qml
@@ -172,6 +172,12 @@
     onPhysicalOrientationChanged: {
         if (!orientationLocked) {
             orientation = physicalOrientation;
+        } else {
+            if (orientation !== physicalOrientation && !shell.showingGreeter) {
+                rotateButton.show()
+            } else {
+                rotateButton.hide()
+            }
         }
     }
     onOrientationLockedChanged: {
@@ -337,6 +343,168 @@
         }
     }
 
+    Rectangle {
+        id: rotateButton
+
+        readonly property real visibleOpacity: 0.8
+        readonly property bool rotateAvailable: root.orientationLocked && root.physicalOrientation !== root.orientation
+
+        anchors.margins: units.gu(3)
+        states: [
+            State {
+                when: !rotateButton.rotateAvailable
+                AnchorChanges {
+                    target: rotateButton
+                    anchors.right: parent.left
+                    anchors.top: parent.bottom
+                }
+            }
+            , State {
+                when: rotateButton.rotateAvailable && root.physicalOrientation == Qt.InvertedLandscapeOrientation
+                AnchorChanges {
+                    target: rotateButton
+                    anchors.left: parent.left
+                    anchors.bottom: parent.bottom
+                }
+            }
+            , State {
+                when: rotateButton.rotateAvailable && root.physicalOrientation == Qt.LandscapeOrientation
+                AnchorChanges {
+                    target: rotateButton
+                    anchors.right: parent.right
+                    anchors.top: parent.top
+                }
+                PropertyChanges {
+                    target: rotateButton
+                    anchors.topMargin: shell.shellMargin
+                }
+            }
+            , State {
+                when: rotateButton.rotateAvailable && root.physicalOrientation == Qt.PortraitOrientation
+                AnchorChanges {
+                    target: rotateButton
+                    anchors.right: parent.right
+                    anchors.bottom: parent.bottom
+                }
+            }
+            , State {
+                when: rotateButton.rotateAvailable && root.physicalOrientation == Qt.InvertedPortraitOrientation
+                AnchorChanges {
+                    target: rotateButton
+                    anchors.left: parent.left
+                    anchors.top: parent.top
+                }
+                PropertyChanges {
+                    target: rotateButton
+                    anchors.topMargin: shell.shellMargin
+                }
+            }
+        ]
+        height: units.gu(4)
+        width: height
+        radius: width / 2
+        visible: opacity > 0
+        opacity: 0
+        color: theme.palette.normal.background
+        border {
+            width: units.dp(1)
+            color: theme.palette.normal.backgroundText
+        }
+
+        function show() {
+            if (!visible) {
+                showDelay.restart()
+            }
+        }
+
+        function hide() {
+            hideAnimation.restart()
+            showDelay.stop()
+        }
+
+        Icon {
+            id: icon
+
+            implicitWidth: units.gu(3)
+            implicitHeight: implicitWidth
+            anchors.centerIn: parent
+            name: "view-rotate"
+            color: theme.palette.normal.backgroundText
+        }
+
+        MouseArea {
+            anchors.fill: parent
+            onClicked: {
+                rotateButton.hide()
+                orientationLock.savedOrientation = root.orientation
+                root.orientation = root.physicalOrientation
+            }
+        }
+
+        LomiriNumberAnimation {
+            id: showAnimation
+
+            running: false
+            property: "opacity"
+            target: rotateButton
+            alwaysRunToEnd: true
+            to: rotateButton.visibleOpacity
+            duration: LomiriAnimation.SlowDuration
+        }
+
+        LomiriNumberAnimation {
+            id: hideAnimation
+
+            running: false
+            property: "opacity"
+            target: rotateButton
+            alwaysRunToEnd: true
+            to: 0
+            duration: LomiriAnimation.FastDuration
+        }
+
+        SequentialAnimation {
+            running: rotateButton.visible
+            loops: 3
+            RotationAnimation {
+                target: rotateButton
+                duration: LomiriAnimation.SnapDuration
+                to: 0
+                direction: RotationAnimation.Shortest
+            }
+            NumberAnimation { target: icon; duration: LomiriAnimation.SnapDuration; property: "opacity"; to: 1 }
+            PauseAnimation { duration: LomiriAnimation.SlowDuration }
+            RotationAnimation {
+                target: rotateButton
+                duration: LomiriAnimation.SlowDuration
+                to: root.orientationLocked ? QtQuickWindow.Screen.angleBetween(root.orientation, root.physicalOrientation) : 0
+                direction: RotationAnimation.Shortest
+            }
+            PauseAnimation { duration: LomiriAnimation.SlowDuration }
+            NumberAnimation { target: icon; duration: LomiriAnimation.SnapDuration; property: "opacity"; to: 0 }
+
+            onFinished: rotateButton.hide()
+        }
+
+        Timer {
+            id: showDelay
+
+            running: false
+            interval: 1000
+            onTriggered: {
+                showAnimation.restart()
+            }
+        }
+
+        Timer {
+            id: hideDelay
+
+            running: false
+            interval: 3000
+            onTriggered: rotateButton.hide()
+        }
+    }
+
     Rectangle {
         id: shellCover
         color: "black"
